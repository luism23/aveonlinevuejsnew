import { createApp } from 'vue'
import App from './App.vue'

import "./styles/global.css"
import "./styles/style.css"
import router from './router'

createApp(App).use(router).use(router).use(router).mount('#app')
