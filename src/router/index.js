import { createRouter, createWebHistory } from 'vue-router'
import Pantalla1View from '../views/Pantalla1View.vue'


const routes = [
  {
    path: '/',
    name: 'home',
    component: Pantalla1View
  },
  {
    path: '/pantalla2View',
    name: 'pantalla2View',
    component: () => import('../views/Pantalla2View.vue')
  },
  {
    path: '/pantalla3View',
    name: 'pantalla3View',
    component: () => import('../views/Pantalla3View.vue')
  },
  {
    path:'/pantalla4View',
    name: '/pantalla4View',
    component: () => import('../views/Pantalla4View.vue')
  },
  {
    path:'/pantalla5View',
    name: '/pantalla5View',
    component: () => import('../views/Pantalla5View.vue')
  },
  {
    path:'/pantalla6View',
    name: '/pantalla6View',
    component: () => import('../views/Pantalla6View.vue')
  },
  {
    path:'/pantalla7View',
    name: '/pantalla7View',
    component: () => import('../views/Pantalla7View.vue')
  },
  {
    path:'/pantalla8View',
    name: '/pantalla8View',
    component: () => import('../views/Pantalla8View.vue')
  },
  {
    path:'/pantalla9View',
    name: '/pantalla9View',
    component: () => import('../views/Pantalla9View.vue')
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
